<?php
/**
 * The  Template for displaying 
 *
 * Template Name: Amat Luxury - Servicios
 * Template Post Type: page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

	<div id="content" class="site-content servicios">
		<div class="fluid-container">
			
		</div><!--end container fluid-->
			
		<div class="container">
			<div class="row banner-servicios">
				<div class="col-md-6">
					<h2><?php if( get_field('titulo') ): ?>
						<?php the_field('titulo'); ?>
					<?php endif; ?></h2>
				</div>
				<div class="col-md-6">
					<?php if( get_field('banner') ): ?>
						<img class="imagen" style="max-width: 100%;" src="<?php the_field('banner'); ?>" />
					<?php endif; ?>
				</div>
			</div>
			<nav>
			  <div class="nav nav-tabs" id="nav-tab" role="tablist">
			    <button class="nav-link active" id="nav-propietario-tab" data-bs-toggle="tab" data-bs-target="#nav-propietario" type="button" role="tab" aria-controls="nav-propietario" aria-selected="true">
			    	<?php if( get_field('servicios_al_propietario') ): ?>
						<?php the_field('servicios_al_propietario'); ?>
					<?php endif; ?>
			    </button>
			    <button class="nav-link" id="nav-comprador-tab" data-bs-toggle="tab" data-bs-target="#nav-comprador" type="button" role="tab" aria-controls="nav-comprador" aria-selected="false">
			    	<?php if( get_field('servicios_al_comprador') ): ?>
						<?php the_field('servicios_al_comprador'); ?>
					<?php endif; ?>
			    </button>
			  </div>
			</nav>
			<div class="tab-content" id="nav-tabContent">
			  <div class="tab-pane fade show active" id="nav-propietario" role="tabpanel" aria-labelledby="nav-propietario-tab">
			  		<!--desk-->
			  		<div class="d-flex align-items-start">
					  <div class="nav flex-column nav-pills me-3" id="v-pills-tab" role="tablist" aria-orientation="vertical">
					    <button class="nav-link active" id="v-pills-Valoracion-tab" data-bs-toggle="pill" data-bs-target="#v-pills-Valoracion" type="button" role="tab" aria-controls="v-pills-Valoracion" aria-selected="true">
					    	<?php if( get_field('tab_valoracion') ): ?>
								<?php the_field('tab_valoracion'); ?>
							<?php endif; ?>
					    </button>
					    <button class="nav-link" id="v-pills-Comercializacion-tab" data-bs-toggle="pill" data-bs-target="#v-pills-Comercializacion" type="button" role="tab" aria-controls="v-pills-Comercializacion" aria-selected="false">
					    	<?php if( get_field('tab_comercializacion') ): ?>
								<?php the_field('tab_comercializacion'); ?>
							<?php endif; ?>
					    </button>
					    <button class="nav-link" id="v-pills-Cierre-tab" data-bs-toggle="pill" data-bs-target="#v-pills-Cierre" type="button" role="tab" aria-controls="v-pills-Cierre" aria-selected="false">
					    	<?php if( get_field('tab_cierre_operacion') ): ?>
								<?php the_field('tab_cierre_operacion'); ?>
							<?php endif; ?>
					    </button>
					    <button class="nav-link" id="v-pills-postventa-tab" data-bs-toggle="pill" data-bs-target="#v-pills-postventa" type="button" role="tab" aria-controls="v-pills-postventa" aria-selected="false">
					    	<?php if( get_field('tab_servicio_postventa') ): ?>
								<?php the_field('tab_servicio_postventa'); ?>
							<?php endif; ?>
					    </button>
					  </div>
					  <div class="tab-content" id="v-pills-tabContent">
					    <div class="tab-pane fade show active" id="v-pills-Valoracion" role="tabpanel" aria-labelledby="v-pills-Valoracion-tab">
					    	<?php if( get_field('contenido_valoracion') ): ?>
								<?php the_field('contenido_valoracion'); ?>
							<?php endif; ?>
					    </div>
					    <div class="tab-pane fade" id="v-pills-Comercializacion" role="tabpanel" aria-labelledby="v-pills-Comercializacion-tab">
					    	<?php if( get_field('contenido_comercializacion') ): ?>
								<?php the_field('contenido_comercializacion'); ?>
							<?php endif; ?>
					    </div>
					    <div class="tab-pane fade" id="v-pills-Cierre" role="tabpanel" aria-labelledby="v-pills-Cierre-tab">
					    	<?php if( get_field('contenido_cierre_operacion') ): ?>
								<?php the_field('contenido_cierre_operacion'); ?>
							<?php endif; ?>
					    </div>
					    <div class="tab-pane fade" id="v-pills-postventa" role="tabpanel" aria-labelledby="v-pills-postventa-tab">
					    	<?php if( get_field('contenido_servicio_postventa') ): ?>
								<?php the_field('contenido_servicio_postventa'); ?>
							<?php endif; ?>
					    </div>
					  </div>
					</div>
					<!--end desk-->
					<!--mobile-->
					<div class="accordion accordion-flush" id="accordionFlush1">
					  <div class="accordion-item">
					    <h2 class="accordion-header" id="flush-headingOne">
					      <button class="accordion-button " type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
					        <?php if( get_field('tab_valoracion') ): ?>
								<?php the_field('tab_valoracion'); ?>
							<?php endif; ?>
					      </button>
					    </h2>
					    <div id="flush-collapseOne" class="accordion-collapse collapse show" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlush1">
					      <div class="accordion-body">
					      	<?php if( get_field('contenido_valoracion') ): ?>
								<?php the_field('contenido_valoracion'); ?>
							<?php endif; ?>
					      </div>
					    </div>
					  </div>
					  <div class="accordion-item">
					    <h2 class="accordion-header" id="flush-headingTwo">
					      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
					        <?php if( get_field('tab_comercializacion') ): ?>
								<?php the_field('tab_comercializacion'); ?>
							<?php endif; ?>
					      </button>
					    </h2>
					    <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlush1">
					      <div class="accordion-body">
					      	<?php if( get_field('contenido_comercializacion') ): ?>
								<?php the_field('contenido_comercializacion'); ?>
							<?php endif; ?>
					      </div>
					    </div>
					  </div>
					  <div class="accordion-item">
					    <h2 class="accordion-header" id="flush-headingThree">
					      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
					        <?php if( get_field('tab_cierre_operacion') ): ?>
								<?php the_field('tab_cierre_operacion'); ?>
							<?php endif; ?>
					      </button>
					    </h2>
					    <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlush1">
					      <div class="accordion-body">
					      	<?php if( get_field('contenido_cierre_operacion') ): ?>
								<?php the_field('contenido_cierre_operacion'); ?>
							<?php endif; ?>
					      </div>
					    </div>
					  </div>
					  <div class="accordion-item">
					    <h2 class="accordion-header" id="flush-headingFour">
					      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFour" aria-expanded="false" aria-controls="flush-collapseFour">
					        <?php if( get_field('tab_servicio_postventa') ): ?>
								<?php the_field('tab_servicio_postventa'); ?>
							<?php endif; ?>
					      </button>
					    </h2>
					    <div id="flush-collapseFour" class="accordion-collapse collapse" aria-labelledby="flush-headingFour" data-bs-parent="#accordionFlush1">
					      <div class="accordion-body">
					      	<?php if( get_field('contenido_servicio_postventa') ): ?>
								<?php the_field('contenido_servicio_postventa'); ?>
							<?php endif; ?>
					      </div>
					    </div>
					  </div>
					</div>
					<!--end mobile-->

				</div>
			  <div class="tab-pane fade" id="nav-comprador" role="tabpanel" aria-labelledby="nav-comprador-tab">
			  		<!--desk-->
			  		<div class="d-flex align-items-start">
					  <div class="nav flex-column nav-pills me-3" id="v-pills-tab" role="tablist" aria-orientation="vertical">
					    <button class="nav-link active" id="v-pills-advisor-tab" data-bs-toggle="pill" data-bs-target="#v-pills-advisor" type="button" role="tab" aria-controls="v-pills-advisor" aria-selected="true">
					    	<?php if( get_field('tab_servicio_advisor') ): ?>
								<?php the_field('tab_servicio_advisor'); ?>
							<?php endif; ?>
						</button>
					    <button class="nav-link" id="v-pills-master-tab" data-bs-toggle="pill" data-bs-target="#v-pills-master" type="button" role="tab" aria-controls="v-pills-master" aria-selected="false">
					    	<?php if( get_field('tab_servicio_master') ): ?>
								<?php the_field('tab_servicio_master'); ?>
							<?php endif; ?>
					    </button> 
					  </div>
					  <div class="tab-content" id="v-pills-tabContent">
					    <div class="tab-pane fade show active" id="v-pills-advisor" role="tabpanel" aria-labelledby="v-pills-advisor-tab">
					    	<?php if( get_field('contenido_servicio_advisor') ): ?>
								<?php the_field('contenido_servicio_advisor'); ?>
							<?php endif; ?>
					    </div>
					    <div class="tab-pane fade" id="v-pills-master" role="tabpanel" aria-labelledby="v-pills-master-tab">
					    	<?php if( get_field('contenido_servicio_master') ): ?>
								<?php the_field('contenido_servicio_master'); ?>
							<?php endif; ?>
					    </div>
					  </div>
					</div>
					<!--end desk-->
					<!--mobile-->
					<div class="accordion accordion-flush" id="accordionFlush2">
					  <div class="accordion-item">
					    <h2 class="accordion-header" id="flush-headingOne">
					      <button class="accordion-button " type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
					        <?php if( get_field('tab_servicio_advisor') ): ?>
								<?php the_field('tab_servicio_advisor'); ?>
							<?php endif; ?>
					      </button>
					    </h2>
					    <div id="flush-collapseOne" class="accordion-collapse collapse show" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlush2">
					      <div class="accordion-body">
					      	<?php if( get_field('contenido_servicio_advisor') ): ?>
								<?php the_field('contenido_servicio_advisor'); ?>
							<?php endif; ?>
							</div>
					    </div>
					  </div>
					  <div class="accordion-item">
					    <h2 class="accordion-header" id="flush-headingTwo">
					      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
					        <?php if( get_field('tab_servicio_master') ): ?>
								<?php the_field('tab_servicio_master'); ?>
							<?php endif; ?>
					      </button>
					    </h2>
					    <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlush2">
					      <div class="accordion-body">
					      	<?php if( get_field('contenido_servicio_advisor') ): ?>
								<?php the_field('contenido_servicio_advisor'); ?>
							<?php endif; ?>
					      </div>
					    </div>
					  </div>
					</div>
					<!--end mobile-->
				</div>
			</div>

		</div><!--end container-->
	</div>


<?php
get_footer();