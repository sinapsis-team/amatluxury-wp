<?php

/**
 * The  Template for displaying 
 *
 * Template Name: Amat Luxury - Home block WIDGET
 * Template Post Type: page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<?php wp_head(); ?>
	<!--tipography-->
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Crimson+Text:ital,wght@0,400;0,600;0,700;1,400;1,600;1,700&family=Work+Sans:ital,wght@0,300;0,400;0,500;0,700;1,300;1,400;1,500;1,700&display=swap" rel="stylesheet">
	<!-- Slick Slider -->
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
	<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
	<!--librerías-->
	<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
	<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

	<base href="https://amatluxury.com" target="_PARENT">
</head>

<?php
$navbar_scheme   = get_theme_mod('navbar_scheme', 'navbar-light bg-light'); // Get custom meta-value.
$navbar_position = get_theme_mod('navbar_position', 'static'); // Get custom meta-value.

$search_enabled  = get_theme_mod('search_enabled', '1'); // Get custom meta-value.
?>

<body <?php body_class(); ?>>

	<?php wp_body_open(); ?>


	<div id="content" class="site-content home-block">
		<div class="fluid-container">

		</div><!--end container fluid-->

		<div class="container">
			<div class="row titulo-principal">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<?php if (get_field('titulo')) : ?>
						<h1><?php the_field('titulo'); ?></h1>
					<?php endif; ?>
				</div>
				<div class="col-md-2"></div>
			</div>

			<!--slider-->
			<div class="titulo-slider">
				<?php if (get_field('titulo_slider')) : ?>
					<h3 class="subtitulo"><?php the_field('titulo_slider'); ?></h3>
				<?php endif; ?>
			</div>
			<div id="slider">
				<?php while (have_rows('imagenes_del_slider')) : the_row(); ?>
					<div class="">
						<?php if (get_row_layout() == 'imagenes') : ?>
							<a href="<?php the_sub_field('url_imagen'); ?>"><img class="item" src="<?php the_sub_field('imagen'); ?>" /></a>
						<?php endif; ?>
					</div>
				<?php endwhile; ?>
			</div>
			<div class="btn-slider">
				<a href="<?php the_field('btn_url'); ?>" class="button black">
					<?php if (get_field('boton_servicios')) : ?>
						<?php the_field('boton_servicios'); ?>
					<?php endif; ?>
				</a>
			</div>
			<!--end slider-->

			<!--Propiedades oportunidad-->
			<div class="titulo-oportunidades">
				<?php if (get_field('titulo_oportunidades')) : ?>
					<h3><?php the_field('titulo_oportunidades'); ?></h3>
				<?php endif; ?>
			</div>
			<div class="texto-oportunidades">
				<?php if (get_field('texto_oportunidades')) : ?>
					<h3><?php the_field('texto_oportunidades'); ?></h3>
				<?php endif; ?>
			</div>
			<div class="row oportunidad-grid">
				<?php while (have_rows('oportunidades_bloque')) : the_row(); ?>
					<?php if (get_row_layout() == 'oportunidad') : ?>
						<div class="col-md-6 ">
							<a href="<?php the_sub_field('url'); ?>" class="url">
								<img class="img" src="<?php the_sub_field('imagen'); ?>" />
								<p><?php the_sub_field('texto_de_la_oprtunidad'); ?></p>
							</a>
						</div>
					<?php endif; ?>
				<?php endwhile; ?>
			</div><!--end Propiedades oportunidad-->

			<div class="row block-secnd">
				<div class="col-md-6 diferente">
					<!-- bloque amat diferente-->
					<div class="titulo-diferente">
						<?php if (get_field('titulo_amat_diferente')) : ?>
							<h3><?php the_field('titulo_amat_diferente'); ?></h3>
						<?php endif; ?>
					</div>
					<div class="row block-dif">
						<?php while (have_rows('bloque_amat_diferente')) : the_row(); ?>
							<?php if (get_row_layout() == 'diferenciacion') : ?>
								<div class="col-md-6" style="  margin-top: 3rem;">
									<img class="icon" src="<?php the_sub_field('icono'); ?>" />
									<h4 class="titulo"><?php the_sub_field('titulo_diferenciacion'); ?></h4>
									<p><?php the_sub_field('texto_diferenciacion'); ?></p>
								</div>
							<?php endif; ?>
						<?php endwhile; ?>
					</div>
					<!--end bloque amat diferente-->
				</div>
				<div class="col-md-6 villae-block">
					<!-- bloque villae-->
					<div class="row ">
						<?php while (have_rows('bloque_villae')) : the_row(); ?>
							<?php if (get_row_layout() == 'villae_magazine') : ?>
								<h3><?php the_sub_field('titulo_villae_magazine'); ?></h3>
								<div class="col-md-6">
									<!--<img class="imagen" src="<?php the_sub_field('imagen'); ?>" />-->
									<div id="villae-slider" class="villae-slider-img">
										<?php while (have_rows('imagenes_slider')) : the_row(); ?>
											<?php if (get_row_layout() == 'imagenes') : ?>

												<img class="icon" src="<?php the_sub_field('imagen'); ?>" />

											<?php endif; ?>
										<?php endwhile; ?>
									</div>
								</div>
								<div class="col-md-6">
									<h4 class="texto"><?php the_sub_field('texto'); ?></h4>
									<a href="<?php the_sub_field('url_solicitar'); ?>" class="url button black">
										<?php the_sub_field('boton_solicitar'); ?>
									</a>
									<a href="<?php the_sub_field('url_boton_online'); ?>" class="url button white" target="_blank">
										<?php the_sub_field('boton_online'); ?>
									</a>
								</div>
							<?php endif; ?>
						<?php endwhile; ?>
					</div><!-- end bloque villae-->
				</div>
			</div>
		</div><!--end container-->
	</div>

	<script>
		jQuery(document).ready(function() {
			jQuery('#slider').slick({
				infinite: true,
				slidesToShow: 1,
				slidesToScroll: 1,
				dots: true,
				arrows: false,
				draggable: true
			});
			jQuery('#villae-slider').slick({
				infinite: true,
				slidesToShow: 1,
				slidesToScroll: 1,
				dots: false,
				arrows: false,
				draggable: true
			});
		});
	</script>
</body>

</html>