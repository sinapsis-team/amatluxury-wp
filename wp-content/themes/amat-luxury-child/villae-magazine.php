<?php
/**
 * The  Template for displaying 
 *
 * Template Name: Amat Luxury - Villae magazine
 * Template Post Type: page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

	<div id="content" class="site-content villae-magazine">
		<div class="fluid-container">
			
		</div><!--end container fluid-->
			
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<div class="titulo-principal">
						<?php if( get_field('titulo_principal') ): ?>
							<h1 ><?php the_field('titulo_principal'); ?></h1>
						<?php endif; ?>
					</div>
					<div class="contenido-villae">
						<?php if( get_field('contenido_villae') ): ?>
							<p><?php the_field('contenido_villae'); ?></p>
						<?php endif; ?>
					</div>
					<div class="solicitar-catalogo">
						<a class="button black" target="_blank" href="<?php the_field('url_boton_online'); ?>"> 
							<?php if( get_field('boton_online') ): ?>
							<?php the_field('boton_online'); ?>
							<?php endif; ?>
						</a>
						<a class="button white" href="#form">
							<?php if( get_field('boton_solicitar') ): ?>
							<?php the_field('boton_solicitar'); ?>
							<?php endif; ?>
						</a> 
					</div>
				</div>
				<div class="col-md-4">
				</div>
			</div><!--end row-->
			<br>
			<div class="row contact-form-villae">
				<div class="col-md-4 col-12">
					<div id="slider">
						<?php while( have_rows('slider') ): the_row(); ?>
							<div class="">
									<?php if( get_row_layout() == 'imagen_slider' ): ?>
										<img class="item" src="<?php the_sub_field('imagen'); ?>" />
									<?php endif; ?>
							</div>
						<?php endwhile; ?> <!----->
					</div>
					<div class="solicitar-catalogo mbl">
						<a class="button black" target="_blank" href="https://issuu.com/eren/docs/aaff_villae_16_digital_amat">
							<?php if( get_field('boton_online') ): ?>
							<?php the_field('boton_online'); ?>
							<?php endif; ?>
						</a>
						<a class="button white" href="#form">
							<?php if( get_field('boton_solicitar') ): ?>
							<?php the_field('boton_solicitar'); ?>
							<?php endif; ?>
						</a> 
					</div>
				</div>

				<div class="col-md-8 col-12 ">
					<div id="form" class="form">
						<h2 class="villae"><?php esc_html_e( 'Request magazine', 'amat-luxury' ); ?></h2>
						<small>*<?php esc_html_e( 'Required fields', 'amat-luxury' ); ?></small>
						<div ><?php echo do_shortcode( '[contact-form-7 id="71" title="Villae magazine"]' ); ?>
						</div>
					</div>
				</div>
				<br>
			</div><!--end row-->
		</div><!--end container-->
	</div>

<script>
	jQuery(document).ready(function(){
		jQuery('#slider').slick({
		  infinite: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: false,
          arrows: false,
          draggable: true
        });
	});
</script>
<?php

get_footer();