<?php
/**
 * The  Template for displaying 
 *
 * Template Name: Amat Luxury - Plantilla Genérica
 * Template Post Type: page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

	<div id="content" class="site-content p-generica">

		<div class="fluid-container">
			<div class="banner">
				<?php if( get_field('imagen') ): ?>
					<img class="banner-imagen" style="max-width: 100%;" src="<?php the_field('imagen'); ?>" />
				<?php endif; ?>
			</div>
		</div><!--end container fluid-->
		
		<div class="container">
			<div class="col-md-12">
				<div class="titulo-principal">
					<?php if( get_field('titulo_principal') ): ?>
						<h1 ><?php the_field('titulo_principal'); ?></h1>
					<?php endif; ?>
				</div>
				<div class="texto">
					<?php if( get_field('texto') ): ?>
						<?php the_field('texto'); ?>
					<?php endif; ?>
				</div>
				<div class="video">
					<?php if( get_field('video') ): ?>
							<div class="video-container">
						    	<iframe width="100%" height="600" style="border: 0;" src="https://www.youtube.com/embed/<?php the_field('video'); ?>?autoplay=1&controls=1&disablekb=1&modestbranding=1&rel=0&showinfo=0"></iframe>
							</div>
					<?php endif; ?>
				</div>
			
			</div>
		</br>
		</div><!--end container-->
	</div>


<?php
get_footer();