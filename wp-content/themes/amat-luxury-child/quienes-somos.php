<?php
/**
 * The  Template for displaying 
 *
 * Template Name: Amat Luxury - Quiénes somos
 * Template Post Type: page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

	<div id="content" class="site-content quienes-somos">
		<div class="fluid-container">
		</div>

	<div class="container">
			<div class="row banner-quien">
				<div class="col-md-8 ">
					<h2><?php if( get_field('titulo') ): ?>
						<?php the_field('titulo'); ?>
					<?php endif; ?></h2>
				</div>
				<div class="col-md-4">
					<?php if( get_field('imagen') ): ?>
						<img class="imagen" style="max-width: 100%;" src="<?php the_field('imagen'); ?>" />
					<?php endif; ?>
				</div>
			</div>


		<div class="row quienes-somos-section">
				<div class="col-lg-12 col-xs-12 col-md-12">
					<div class="contenido-quien">
						<?php if( get_field('frase_destacada') ): ?>
								<div class="texto-destacado"><?php the_field('frase_destacada'); ?></div>
						<?php endif; ?>
						<?php if( get_field('contenido') ): ?>
								<div class="contenido"><?php the_field('contenido'); ?></div>
						<?php endif; ?>
					</div>
				</div>
				<div class="col-md-12 saber-mas-amat">
				<a class="button gold" target="_blank" href="<?php the_field('url_boton'); ?>"> 
					<?php if( get_field('boton') ): ?>
					<?php the_field('boton'); ?>
					<?php endif; ?>
				</a>
				</div>
		</div><!--end row-->
	</div><!--container-->
	</div>
<?php

get_footer();