<?php
/**
 * Sidebar Template.
 */

if ( is_active_sidebar( 'primary_widget_area' ) || is_archive() || is_single() ) :
?>
<div id="sidebar" class="col-md-12 order-md-first col-sm-12 oder-sm-last">
	<?php
		if ( is_active_sidebar( 'primary_widget_area' ) ) :
	?>
		<div id="widget-area" class="widget-area" role="complementary">
			<?php
				dynamic_sidebar( 'primary_widget_area' );

				if ( current_user_can( 'manage_options' ) ) :
			?>
				<span class="edit-link"><a href="<?php echo esc_url( admin_url( 'widgets.php' ) ); ?>" class="badge badge-secondary"><?php esc_html_e( 'Edit', 'amat-luxury' ); ?></a></span><!-- Show Edit Widget link -->
			<?php
				endif;
			?>
		</div><!-- /.widget-area -->
	<?php
		endif;

		if ( is_archive() || is_single() ) :
	?>
		<div class="bg-faded sidebar-nav">
			<h3><?php esc_html_e( 'Recommended post', 'amat-luxury' ); ?></h3>
			<div id="primary-two" class="widget-area">
				<div class="row lista-post ">
				    	<?php 
							$args = array( 'posts_per_page' => '2' );
							$recent_posts = new WP_Query($args);
							while( $recent_posts->have_posts() ) :  
							    $recent_posts->the_post() ?>
							    <div class="post-recent col-md-12">
							    	<a href="<?php echo get_permalink() ?>">
								    	<?php if ( has_post_thumbnail() ) : ?>
								            <?php the_post_thumbnail('thumbnail') ?>
								        <?php endif ?> 
							        </a>  
							        <br>
							        <div class="box">
							        	<div class="post-info row"> 
								        	<p class="author col-md-6"><span>Por</span> <?php echo get_the_author_meta('display_name', $author_id); ?></p>
								        	<p class="date col-md-6"><?php echo get_the_date( 'j F, Y' ); ?></p>
							        	</div>
							        	<div class="row">
							        		<a class="post-title" href="<?php echo get_permalink() ?>"><?php the_title() ?></a>
							        	</div>
							        	<div class="row">
							        		<?php the_excerpt(); ?>
							        	</div>
							        	<div class="row">
							        		<a class="read-more" href="<?php echo get_permalink() ?>"><?php esc_html_e( 'Read more', 'amat-luxury' ); ?><img src="<?php echo get_site_url(); ?>/wp-content/uploads/2021/11/readmore.png"/></a>
							        	</div>
							        </div>

							    </div>
							<?php endwhile; ?>
							<?php wp_reset_postdata();  ?> 
						</div>
				<br />
				
			</div><!-- /#primary-two -->
		</div>
	<?php
		endif;
	?>
</div><!-- /#sidebar -->
<?php
	endif;
?>
