��          �   %   �      `  ,   a     �     �     �  
   �     �     �     �  a   �     J     O     [     l     x     �     �     �  	   �     �     �     �     �  M   �  )   F  $   p  ,   �  c  �  -   &     T     d  	   w     �     �     �     �  x   �     0	     4	     J	     f	     ~	     �	     �	     �	     �	     �	     �	     �	     
  q   
  7   �
  -   �
     �
     
                                            	                                                                           &copy; 2022 — Amat Immobiliaris since 1948 About us Ask for valuation Comment Contact us Cookies Policy I am interested Legal Notice Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a> Name Newer posts No Comments yet! Older posts Online administration Our Offices Post Comment Privacy Policy Read more Recommended post Reply Request magazine Required fields Save my name, email, and website in this browser for the next time I comment. Your Email address will not be published. Your comment is awaiting moderation. comments titleOne Reply to &ldquo;%s&rdquo; Project-Id-Version: them.es Starter Theme v1.0
Report-Msgid-Bugs-To: 
POT-Revision-Date: Tue Apr 12 2016 16:05:52 GMT+0200 (CEST)
PO-Revision-Date: 2022-01-27 10:41+0100
Language-Team: 
Plural-Forms: nplurals=2; plural=(n != 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-SourceCharset: utf-8
X-Generator: Poedit 3.0
X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2
X-Poedit-Basepath: ..
X-Textdomain-Support: yes
X-Loco-Target-Locale: de_DE
Last-Translator: 
Language: ca
X-Poedit-SearchPath-0: .
 &copy; 2022 — Amat Immobiliaris des de 1948 Sobre nosaltres Demanar valoració Comentari Contacte Política de Cookies M'interessa Aviso Legal Has iniciat sessió com a <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Tancar la sessió?</a> Nom Publicació més nova Encara no hi ha comentaris! Publicació més antiga Administració online Oficines Escriu un comentari Política de Privacitat Llegeix més Publicació recomanada Resposta Sol·licitar revista Camps obligatoris Desa el meu nom, correu electrònic i lloc web en aquest navegador per a la propera vegada que faci un comentari. La teva adreça de correu electrònic no es publicarà. El teu comentari està pendent de moderació. Una resposta a &ldquo;%s&rdquo; 