<?php
/**
 * The  Template for displaying 
 *
 * Template Name: Amat Luxury - Quiénes somos
 * Template Post Type: page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

	<div id="content" class="site-content quienes-somos">
		<div class="fluid-container">
		</div>

		<div class="container">
			<div class="titulo-principal">
				<?php if( get_field('titulo') ): ?>
					<h1 ><?php the_field('titulo'); ?></h1>
				<?php endif; ?>
			</div>
			<div class="row quienes-somos-section">
				<div class="col-lg-6 col-xs-12 col-md-12">
					<div class="quienes-somos-photo">
						<?php if( get_field('imagen') ): ?>
							<img class="imagen" style="max-width: 100%;" src="<?php the_field('imagen'); ?>" />
						<?php endif; ?>
					</div>
				</div>
				<div class="col-lg-6 col-xs-12 col-md-12">
					<div class="contenido">
						<?php if( get_field('contenido') ): ?>
								<div class="contenido-oculto"><?php the_field('contenido'); ?></div>
							<?php endif; ?>
					</div>
					<div class="accordion accordion-flush" id="accordion-quienes-somos">
					  <div class="accordion-item">
					    <h2 class="accordion-header" id="flush-headingOne">
					      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
					        <p><?php esc_html_e( 'Read more', 'amat-luxury' ); ?></p>
					      </button>
					    </h2>
					    <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordion-quienes-somos">
					      <div class="accordion-body">
					      	<?php if( get_field('contenido_oculto') ): ?>
								<div class="contenido-oculto"><?php the_field('contenido_oculto'); ?></div>
							<?php endif; ?>

							<div class="accordion accordion-flush" id="accordion-tab-1">
							  <div class="accordion-item">
							    <h2 class="accordion-header" id="flush-headingTwo">
							      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
							        <?php if( get_field('titulo_tab_1') ): ?>
										<div class="titulo_tab_1"><?php the_field('titulo_tab_1'); ?></div>
									<?php endif; ?>
							      </button>
							    </h2>
							    <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordion-tab-1">
							      <div class="accordion-body">
							      	<?php if( get_field('contenido_tab_1') ): ?>
										<div class="contenido_tab_1"><?php the_field('contenido_tab_1'); ?></div>
									<?php endif; ?>
									<?php if( get_field('imagen_tab_1') ): ?>
										<div class="image"><img class="imagen_tab_1" src="<?php the_field('imagen_tab_1'); ?>" /></div>
									<?php endif; ?>
							      </div>
							    </div>
							  </div>
							  <div class="accordion-item">
							    <h2 class="accordion-header" id="flush-headingThree">
							      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
							        <?php if( get_field('titulo_tab_2') ): ?>
										<div class="titulo_tab_2"><?php the_field('titulo_tab_2'); ?></div>
									<?php endif; ?>
							      </button>
							    </h2>
							    <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordion-tab-2">
							      <div class="accordion-body">
							      	<?php if( get_field('contenido_tab_2') ): ?>
										<div class="contenido_tab_2"><?php the_field('contenido_tab_2'); ?></div>
									<?php endif; ?>
							      </div>
							    </div>
							  </div>
							  <div class="accordion-item">
							    <h2 class="accordion-header" id="flush-headingFour">
							      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFour" aria-expanded="false" aria-controls="flush-collapseOne">
							        <?php if( get_field('titulo_tab_3') ): ?>
										<div class="titulo_tab_3"><?php the_field('titulo_tab_3'); ?></div>
									<?php endif; ?>
							      </button>
							    </h2>
							    <div id="flush-collapseFour" class="accordion-collapse collapse" aria-labelledby="flush-headingFour" data-bs-parent="#accordion-tab-3">
							      <div class="accordion-body">
							      	<?php if( get_field('contenido_tab_3') ): ?>
										<div class="contenido_tab_3"><?php the_field('contenido_tab_3'); ?></div>
									<?php endif; ?>
							      </div>
							    </div>
							  </div>
							</div><!--end accordion-->



					      </div>
					    </div>
					  </div>
					</div><!--end accordion-->
				</div>
			</div><!--end row-->
		</div><!--container-->
	</div>
<?php

get_footer();