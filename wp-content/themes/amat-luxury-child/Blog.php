<?php
/**
 * The  Template for displaying 
 *
 * Template Name: Amat Luxury - Blog pag. principal
 * Template Post Type: page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

	<div id="content" class="site-content blog">
		<div class="fluid-container">
			
		</div><!--end container fluid-->
			
		<div class="container">
			 <!--mostrar post-->
		   
				    <div class="row">
				    	<div class="col-md-12 titulo-seccion">
			                <h2></h2>
			            </div>
			        </div>
			        <div class="">
			            <div class="row lista-post ">
				    	<?php 
							$args = array( 'posts_per_page' => '8' );
							$recent_posts = new WP_Query($args);
							while( $recent_posts->have_posts() ) :  
							    $recent_posts->the_post() ?>
							    <div class="post-recent col-md-4">
							    	<a href="<?php echo get_permalink() ?>">
								    	<?php if ( has_post_thumbnail() ) : ?>
								            <?php the_post_thumbnail('thumbnail') ?>
								        <?php endif ?> 
							        </a>  
							        <br>
							        <div class="box">
							        	<div class="post-info row"> 
								        	<p class="author col-md-6"><span>Por</span> <?php echo get_the_author_meta('display_name', $author_id); ?></p>
								        	<p class="date col-md-6"><?php echo get_the_date( 'j F, Y' ); ?></p>
							        	</div>
							        	<div class="row">
							        		<a class="post-title" href="<?php echo get_permalink() ?>"><?php the_title() ?></a>
							        	</div>
							        	<div class="row text-excerpt">
							        		<?php the_excerpt(); ?>
							        	</div>
							        	<div class="row">
							        		<a class="read-more" href="<?php echo get_permalink() ?>"><?php esc_html_e( 'Read more', 'amat-luxury' ); ?><img src="<?php echo get_site_url(); ?>/wp-content/uploads/2021/11/readmore.png"/></a>
							        	</div>
							        </div>

							    </div>
							<?php endwhile; ?>
							<?php wp_reset_postdata();  ?> 
						</div>
					
				    </div>
				
		    <!--end mostrar posts-->

		</div><!--end container-->
	</div>


<?php
get_footer();