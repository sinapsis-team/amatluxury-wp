<footer id="footer">
	<div class="container">
		<div class="row footer">
			<div class="col-md-2 footer1 ">
				<img class="logo" src="<?php echo get_site_url(); ?>/wp-content/uploads/2021/10/logo.png" />
			</div>
			<div class="col-md-3 footer5 mobile">
				<div class="first-line">
					<a target="_blank" href=""><img class="" src="<?php echo get_site_url(); ?>/wp-content/uploads/2021/11/nac-premier.png" /></a>
					<a target="_blank" href=""><img class="" src="<?php echo get_site_url(); ?>/wp-content/uploads/2021/11/lluxury.png" /></a>
					<a target="_blank" href=""><img class="" src="<?php echo get_site_url(); ?>/wp-content/uploads/2021/11/mayfer.png" /></a>
				</div>
				<div class="snd-line" style="text-align: center;">
					<!--<a target="_blank" href=""><img class="" src="<?php echo get_site_url(); ?>/wp-content/uploads/2021/11/eren.png"/></a>-->
					<a target="_blank" href=""><img class="" src="<?php echo get_site_url(); ?>/wp-content/uploads/2021/11/fiabci.png" /></a>
				</div>
			</div>
			<div class="col-md-2 footer2 ">
				<?php if (ICL_LANGUAGE_CODE == 'en') { ?>
					<p><a class="footer-title" href="/about-us"><?php esc_html_e('About us', 'amat-luxury'); ?></a></p>
				<?php } elseif (ICL_LANGUAGE_CODE == 'ca') { ?>
					<p><a class="footer-title" href="/ca/qui-som/"><?php esc_html_e('About us', 'amat-luxury'); ?></a></p>
				<?php } elseif (ICL_LANGUAGE_CODE == 'es') { ?>
					<p><a class="footer-title" href="/es/quienes-somos"><?php esc_html_e('About us', 'amat-luxury'); ?></a></p>
				<?php } ?>

			</div>
			<div class="col-md-2 footer3 ">
				<?php if (ICL_LANGUAGE_CODE == 'en') { ?>
					<p><a class="footer-title" href="/blog/contact"><?php esc_html_e('Our Offices', 'amat-luxury'); ?></a></p>
				<?php } elseif (ICL_LANGUAGE_CODE == 'ca') { ?>
					<p><a class="footer-title" href="/blog/ca/contacte"><?php esc_html_e('Our Offices', 'amat-luxury'); ?></a></p>
				<?php } elseif (ICL_LANGUAGE_CODE == 'es') { ?>
					<p><a class="footer-title" href="/blog/es/contacto"><?php esc_html_e('Our Offices', 'amat-luxury'); ?></a></p>
				<?php } ?>

				<?php if (ICL_LANGUAGE_CODE == 'en') { ?>
					<p><a class="footer-text" href="/blog/contact/#headingFour"><?php esc_html_e('Sant Just Desvern', 'amat-luxury'); ?></a></p>
				<?php } elseif (ICL_LANGUAGE_CODE == 'ca') { ?>
					<p><a class="footer-text" href="/blog/ca/contacte/#headingFour"><?php esc_html_e('Sant Just Desvern', 'amat-luxury'); ?></a></p>
				<?php } elseif (ICL_LANGUAGE_CODE == 'es') { ?>
					<p><a class="footer-text" href="/blog/es/contacto/#headingFour"><?php esc_html_e('Sant Just Desvern', 'amat-luxury'); ?></a></p>
				<?php } ?>

				<?php if (ICL_LANGUAGE_CODE == 'en') { ?>
					<p><a class="footer-text" href="/blog/contact/#headingOne"><?php esc_html_e('Sant Cugat del Vallès', 'amat-luxury'); ?></a></p>
				<?php } elseif (ICL_LANGUAGE_CODE == 'ca') { ?>
					<p><a class="footer-text" href="/blog/ca/contacte/#headingOne"><?php esc_html_e('Sant Cugat del Vallès', 'amat-luxury'); ?></a></p>
				<?php } elseif (ICL_LANGUAGE_CODE == 'es') { ?>
					<p><a class="footer-text" href="/blog/es/contacto/#headingOne"><?php esc_html_e('Sant Cugat del Vallès', 'amat-luxury'); ?></a></p>
				<?php } ?>

				<?php if (ICL_LANGUAGE_CODE == 'en') { ?>
					<p><a class="footer-text" href="/blog/contact/#oficinas"><?php esc_html_e('Barcelona', 'amat-luxury'); ?></a></p>
				<?php } elseif (ICL_LANGUAGE_CODE == 'ca') { ?>
					<p><a class="footer-text" href="/blog/ca/contacte/#oficinas"><?php esc_html_e('Barcelona', 'amat-luxury'); ?></a></p>
				<?php } elseif (ICL_LANGUAGE_CODE == 'es') { ?>
					<p><a class="footer-text" href="/blog/es/contacto/#oficinas"><?php esc_html_e('Barcelona', 'amat-luxury'); ?></a></p>
				<?php } ?>
			</div>
			<div class="col-md-3 footer4 ">

				<?php if (ICL_LANGUAGE_CODE == 'en') { ?>
					<p class="footer-title"><a style="font-size: 16px;" href="/blog/contact"><?php esc_html_e('Contact us', 'amat-luxury'); ?></a></p>
				<?php } elseif (ICL_LANGUAGE_CODE == 'ca') { ?>
					<p class="footer-title"><a style="font-size: 16px;" href="/blog/ca/contacte"><?php esc_html_e('Contact us', 'amat-luxury'); ?></a></p>
				<?php } elseif (ICL_LANGUAGE_CODE == 'es') { ?>
					<p class="footer-title"><a style="font-size: 16px;" href="/blog/es/contacto"><?php esc_html_e('Contact us', 'amat-luxury'); ?></a></p>
				<?php } ?>

				<a href="tel:+34934529960">
					<p>(+34) 934 529 960</p>
				</a>
				<a href="mailto:luxury@amatimmo.cat">
					<p>luxury@amatimmo.cat</p>
				</a>
			</div>
			<div class="col-md-3 footer5 ">
				<div class="first-line">
					<a target="_blank" href="http://www.nacpremier.com/"><img class="" src="<?php echo get_site_url(); ?>/wp-content/uploads/2021/11/nac-premier.png" /></a>
					<a target="_blank" href="https://www.luxuryrealestate.com/"><img class="" src="<?php echo get_site_url(); ?>/wp-content/uploads/2021/11/lluxury.png" /></a>
				</div>
				<div class="snd-line">
					<a target="_blank" href="https://www.mayfairinternationalrealty.com/"><img class="" src="<?php echo get_site_url(); ?>/wp-content/uploads/2021/11/mayfer.png" /></a>
					<a target="_blank" href="http://www.fiabcispain.com/"><img class="" src="<?php echo get_site_url(); ?>/wp-content/uploads/2021/11/fiabci.png" /></a>
				</div>
			</div>
		</div>
		<div class="row black-footer">
			<div class="col-md-3 mobile">
				<div class="rrss">
					<a target="_blank" href="https://twitter.com/amatluxury"><img class="" src="<?php echo get_site_url(); ?>/wp-content/uploads/2021/11/twitter.png" /></a>
					<a target="_blank" href="https://www.facebook.com/amatimmobiliaris"><img class="" src="<?php echo get_site_url(); ?>/wp-content/uploads/2021/11/fb.png" /></a>
					<a target="_blank" href="https://www.instagram.com/amat_immo/"><img class="" src="<?php echo get_site_url(); ?>/wp-content/uploads/2021/11/instagram.png" /></a>
					<a target="_blank" href="https://www.linkedin.com"><img class="" src="<?php echo get_site_url(); ?>/wp-content/uploads/2021/11/linkedin.png" /></a>
				</div>
			</div>
			<div class="col-md-6 links-legales ">
				<?php
				if (has_nav_menu('footer-menu')) : // See function register_nav_menus() in functions.php
					/*
									Loading WordPress Custom Menu (theme_location) ... remove <div> <ul> containers and show only <li> items!!!
									Menu name taken from functions.php!!! ... register_nav_menu( 'footer-menu', 'Footer Menu' );
									!!! IMPORTANT: After adding all pages to the menu, don't forget to assign this menu to the Footer menu of "Theme locations" /wp-admin/nav-menus.php (on left side) ... Otherwise the themes will not know, which menu to use!!!
								*/
					wp_nav_menu(
						array(
							'theme_location'  => 'footer-menu',
							'container'       => 'nav',
							'container_class' => 'col-md-12',
							'fallback_cb'     => '',
							'items_wrap'      => '<ul class="menu nav justify-content-end">%3$s</ul>',
							//'fallback_cb'    => 'WP_Bootstrap4_Navwalker_Footer::fallback',
							'walker'          => new WP_Bootstrap4_Navwalker_Footer(),
						)
					);
				endif;

				if (is_active_sidebar('third_widget_area')) :
				?>
					<div class="col-md-12">
						<?php
						dynamic_sidebar('third_widget_area');

						if (current_user_can('manage_options')) :
						?>
							<span class="edit-link"><a href="<?php echo esc_url(admin_url('widgets.php')); ?>" class="badge badge-secondary"><?php esc_html_e('Edit', 'amat-luxury'); ?></a></span><!-- Show Edit Widget link -->
						<?php
						endif;
						?>
					</div>
				<?php
				endif;
				?>
				<!--end footer menu-->
			</div>
			<div class="col-md-4 reserved">
				<p><?php printf(esc_html__('&copy; 2022 — Amat Immobiliaris since 1948', 'amat-luxury'), date_i18n('Y'), get_bloginfo('name', 'display')); ?></p>
			</div>
			<!--<div class="col-md-1 languages">
						<a></a>
					</div>-->
			<div class="col-md-2 desk">
				<div class="rrss">
					<a target="_blank" href="https://twitter.com/amatluxury"><img class="" src="<?php echo get_site_url(); ?>/wp-content/uploads/2021/11/twitter.png" /></a>
					<a target="_blank" href="https://www.facebook.com/amatimmobiliaris"><img class="" src="<?php echo get_site_url(); ?>/wp-content/uploads/2021/11/fb.png" /></a>
					<a target="_blank" href="https://www.instagram.com/accounts/login/"><img class="" src="<?php echo get_site_url(); ?>/wp-content/uploads/2021/11/instagram.png" /></a>
					<a target="_blank" href="https://www.linkedin.com/company/amat-finques/"><img class="" src="<?php echo get_site_url(); ?>/wp-content/uploads/2021/11/linkedin.png" /></a>
				</div>
			</div>



			<!--footer menu--->
			<!--<?php
				if (has_nav_menu('footer-menu')) : // See function register_nav_menus() in functions.php
					/*
								Loading WordPress Custom Menu (theme_location) ... remove <div> <ul> containers and show only <li> items!!!
								Menu name taken from functions.php!!! ... register_nav_menu( 'footer-menu', 'Footer Menu' );
								!!! IMPORTANT: After adding all pages to the menu, don't forget to assign this menu to the Footer menu of "Theme locations" /wp-admin/nav-menus.php (on left side) ... Otherwise the themes will not know, which menu to use!!!
							*/
					wp_nav_menu(
						array(
							'theme_location'  => 'footer-menu',
							'container'       => 'nav',
							'container_class' => 'col-md-6',
							'fallback_cb'     => '',
							'items_wrap'      => '<ul class="menu nav justify-content-center">%3$s</ul>',
							//'fallback_cb'    => 'WP_Bootstrap4_Navwalker_Footer::fallback',
							'walker'          => new WP_Bootstrap4_Navwalker_Footer(),
						)
					);
				endif;

				if (is_active_sidebar('third_widget_area')) :
				?>
						<div class="col-md-12">
							<?php
							dynamic_sidebar('third_widget_area');

							if (current_user_can('manage_options')) :
							?>
								<span class="edit-link"><a href="<?php echo esc_url(admin_url('widgets.php')); ?>" class="badge badge-secondary"><?php esc_html_e('Edit', 'amat-luxury'); ?></a></span><!-- Show Edit Widget link -->
		<?php
							endif;
		?>
		</div>
		<!--<?php
				endif;
			?>-->
		<!--end footer menu-->
	</div><!-- /.row -->
	</div><!-- /.container -->
</footer><!-- /#footer -->
</div><!-- /#wrapper -->
<?php
wp_footer();
?>

<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('.more-info').click(function(e) {
			e.preventDefault();
			var strAncla = $(this).attr('href');
			$('body,html').stop(true, true).animate({
				scrollTop: $(strAncla).offset().top - 5
			}, 1500);
		});
		/*$(window).scroll(function(){
			var footerForm = $('#footer-form').offset().top + 150;
			var pagina = $(document).scrollTop()+window.innerHeight;
			if (pagina >= footerForm){
				$('.more-info').css({
				  	'display' : 'none'
				});
			} else {
				$('.more-info').css({
				   'display' : 'block'
				});
			}
		});*/
	});
</script>

</body>

</html>