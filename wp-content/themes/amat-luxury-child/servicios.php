<?php
/**
 * The  Template for displaying 
 *
 * Template Name: Amat Luxury - Servicios
 * Template Post Type: page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

	<div id="content" class="site-content servicios">
		<div class="fluid-container">
			
		</div><!--end container fluid-->
			
		<div class="container">
			<div class="row banner-servicios">
				<div class="col-md-12 ">
					<h2><?php if( get_field('titulo') ): ?>
						<?php the_field('titulo'); ?>
					<?php endif; ?></h2>
					<?php if( get_field('subtitulo') ): ?>
						<?php the_field('subtitulo'); ?>
					<?php endif; ?>
					<div class="div-img">
						<?php if( get_field('imagen_1') ): ?>
							<img class="imagen" style="max-width: 100%;" src="<?php the_field('imagen_1'); ?>" />
						<?php endif; ?>
					</div>
				</div>
			</div>
			
			<div class="row td-servicios">

				<?php $a = 0; ?>

				<?php while( have_rows('seccion_servicios') ): the_row(); ?>
					<?php if( get_row_layout() == 'servicios' ): ?>
						<div id="servicio<?=$a++?>" class="row single-service">
							<div class="col-md-6 uno">
								<img class="icon" src="<?php the_sub_field('icono'); ?>" />
								<h1><?php the_sub_field('titulo'); ?></h1>
								<p><?php the_sub_field('texto'); ?></p>
								<a class="button black" href="<?php the_sub_field('url_boton'); ?>"><?php the_sub_field('boton'); ?></a>
							</div>
							<div class="col-md-6 dos">
								<img class="img" src="<?php the_sub_field('imagen'); ?>" />
							</div>
						</div>
						 <? $a++; endforeach; ?>
					<?php endif; ?>
				<?php endwhile; ?>
			</div>

			<!--contacto-->
			<div id="contact" class="card-form">
				<div class="row">
					<div class="col-md-2"></div>
					<div class="col-md-8 formulario">
						<div class="titulo-principal">
							<h1 ><?php esc_html_e( 'Ask for valuation', 'amat-luxury' ); ?></h1>
						</div>
						<div class="form">
							<small>*<?php esc_html_e( 'Required fields', 'amat-luxury' ); ?></small>
							<div class="form "><?php echo do_shortcode( '[contact-form-7 id="10" title="Contacto"]' ); ?>
							</div>
						</div>
					</div>
					<div class="col-md-2"></div>
				</div>
			</div>
			
			<!--end contacto-->

		</div><!--end container-->
	</div>


<?php
get_footer();