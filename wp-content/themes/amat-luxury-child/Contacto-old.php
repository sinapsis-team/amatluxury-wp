<?php
/**
 * The  Template for displaying 
 *
 * Template Name: Amat Luxury - Contacto old
 * Template Post Type: page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

	<div id="content" class="site-content contacto">
		<div class="fluid-container">
			<div class="banner">
				<?php if( get_field('banner_imagen') ): ?>
					<img class="banner-imagen" style="max-width: 100%;" src="<?php the_field('banner_imagen'); ?>" />
				<?php endif; ?>
			</div>
		</div><!--end container fluid-->
			
		<div class="container">
			<div class="row">
				<div class="col-md-2">
				</div>
				<div class="col-md-8">
					<div class="card-form">
						<div class="titulo-principal">
							<?php if( get_field('titulo_principal') ): ?>
								<h1 ><?php the_field('titulo_principal'); ?></h1>
							<?php endif; ?>
						</div>
						<div class="form">
							<small>*<?php esc_html_e( 'Required fields', 'amat-luxury' ); ?></small>
							<div class="form "><?php echo do_shortcode( '[contact-form-7 id="10" title="Contacto"]' ); ?>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-2">
				</div>
			</div>
			<br>
			<div class="oficinas">
				<div class="oficinas-titulo">
						<?php if( get_field('titulo_oficinas') ): ?>
							<h3><?php the_field('titulo_oficinas'); ?></h3>
						<?php endif; ?>
				</div>
			
				<div class="row">
					<div class="col-lg-6 col-xs-12 col-md-12">
						<div class="acordeon-oficina">
							<div class="accordion" id="accordionOne">
							  <div class="accordion-item">
							    <h2 class="accordion-header" id="headingOne">
							      <button class="accordion-button " type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
							        <?php if( get_field('titulo_oficina_1') ): ?>
										<p><?php the_field('titulo_oficina_1'); ?></p>
									<?php endif; ?>
							      </button>
							    </h2>
							    <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionOne">
							      <div class="accordion-body">
							        <div class="direccion-horario">
							        	<?php if( get_field('oficina_1_direccion') ): ?>
										<p><?php the_field('oficina_1_direccion'); ?></p>
										<?php endif; ?>
										<?php if( get_field('oficina_1_horario') ): ?>
											<p><?php the_field('oficina_1_horario'); ?></p>
										<?php endif; ?>
									</div>
									<iframe width="450" height="450" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="<?php the_field('mapa_1'); ?>" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
							      </div>
							    </div>
							  </div>
							</div>
						</div><!--end acordion-->
						<div class="acordeon-oficina">
							<div class="accordion" id="accordionThree">
							  <div class="accordion-item">
							    <h2 class="accordion-header" id="headingOne">
							      <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
							        <?php if( get_field('titulo_oficina_3') ): ?>
										<p><?php the_field('titulo_oficina_3'); ?></p>
									<?php endif; ?>
							      </button>
							    </h2>
							    <div id="collapseThree" class="accordion-collapse collapse show" aria-labelledby="headingThree" data-bs-parent="#accordionThree">
							      <div class="accordion-body">
							      	<div class="direccion-horario">
								        <?php if( get_field('oficina_3_direccion') ): ?>
											<p><?php the_field('oficina_3_direccion'); ?></p>
										<?php endif; ?>
										<?php if( get_field('oficina_3_horario') ): ?>
											<p><?php the_field('oficina_3_horario'); ?></p>
										<?php endif; ?>
									</div>
									<iframe width="450" height="450" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="<?php the_field('mapa_3'); ?>" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
							      </div>
							    </div>
							  </div>
							</div>
						</div><!--end acordion-->
					</div><!--end row-->
					<div class="col-lg-6 col-xs-12 col-md-12">
						<div class="acordeon-oficina">
							<div class="accordion" id="accordionTwo">
							  <div class="accordion-item">
							    <h2 class="accordion-header" id="headingTwo">
							      <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
							        <?php if( get_field('titulo_oficina_2') ): ?>
										<p><?php the_field('titulo_oficina_2'); ?></p>
									<?php endif; ?>
							      </button>
							    </h2>
							    <div id="collapseTwo" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionTwo">
							      <div class="accordion-body">
							      	<div class="direccion-horario">
								        <?php if( get_field('oficina_2_direccion') ): ?>
											<p><?php the_field('oficina_2_direccion'); ?></p>
										<?php endif; ?>
										<?php if( get_field('oficina_2_horario') ): ?>
											<p><?php the_field('oficina_2_horario'); ?></p>
										<?php endif; ?>
									</div>
									<iframe width="450" height="450" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="<?php the_field('mapa_2'); ?>" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
							      </div>
							    </div>
							  </div>
							</div>
						</div><!--end acordion-->
						<div class="acordeon-oficina">
							<div class="accordion" id="accordionFour">
							  <div class="accordion-item">
							    <h2 class="accordion-header" id="headingFour">
							      <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
							        <?php if( get_field('titulo_oficina_4') ): ?>
										<p><?php the_field('titulo_oficina_4'); ?></p>
									<?php endif; ?>
							      </button>
							    </h2>
							    <div id="collapseFour" class="accordion-collapse collapse show" aria-labelledby="headingFour" data-bs-parent="#accordionFour">
							      <div class="accordion-body">
							        <div class="direccion-horario">
							        	<?php if( get_field('oficina_4_direccion') ): ?>
										<p><?php the_field('oficina_4_direccion'); ?></p>
										<?php endif; ?>
										<?php if( get_field('oficina_4_horario') ): ?>
											<p><?php the_field('oficina_4_horario'); ?></p>
										<?php endif; ?>
									</div>
									<iframe width="450" height="450" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="<?php the_field('mapa_4'); ?>" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
							      </div>
							    </div>
							  </div>
							</div>
						</div><!--end acordion 4-->
					</div><!--end row-->
				</div><!--end principal row-->
			</div>
		</div><!--end container-->
	</div>
<?php

get_footer();