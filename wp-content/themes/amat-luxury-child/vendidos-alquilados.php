<?php

/**
 * The  Template for displaying 
 *
 * Template Name: Amat Luxury - Vendidos y alquilados
 * Template Post Type: page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

<div id="content" class="site-content vendidos-alquilados">
	<div class="fluid-container">

	</div>
	<!--end container fluid-->

	<div class="container">
		<div class="col-md-8">
			<div class="titulo-principal">
				<?php if (get_field('titulo_principal')) : ?>
					<h1><?php the_field('titulo_principal'); ?></h1>
				<?php endif; ?>
			</div>
			<div class="contenido-vend-alq">
				<?php if (get_field('texto_explicativo')) : ?>
					<p><?php the_field('texto_explicativo'); ?></p>
				<?php endif; ?>
			</div>
		</div>
		<div class="col-md-4">
		</div>
		<div class="casa-grid">
			<div class="row ">
				<?php while (have_rows('ficha_casa')) : the_row(); ?>
					<?php if (get_row_layout() == 'anadir_casa') : ?>
						<div class="col-md-4 casa">
							<img class="img-casa" src="<?php the_sub_field('imagen'); ?>" />
							<h4 class="tag"><?php the_sub_field('etiqueta_vendido'); ?></h4>
							<div class="info-casa">
								<h4 class="title"><?php the_sub_field('titulo_casa'); ?></h4>
								<p class="ciudad"><?php the_sub_field('ciudad'); ?></p>
								<div class="icons-fil">
									<?php while (have_rows('fila_iconos')) : the_row(); ?>
										<?php if (get_row_layout() == 'iconos') : ?>
											<div class="col-md-2 info-ic">
												<img class="icon" src="<?php the_sub_field('icono'); ?>" />
												<p class="nmb"><?php the_sub_field('numero'); ?></p>
											</div>
										<?php endif; ?>
									<?php endwhile; ?>
								</div>
							</div>
						</div>
					<?php endif; ?>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
	<!--end container-->
</div>


<script>
	jQuery(document).ready(function($){
        $('.wpml-lang-es .casa-grid .tag').each(function () {
            if ($(this).text() == 'sold'){
                $(this).text( 'Vendido' );
            } else if ($(this).text() == 'rented'){
                $(this).text( 'Alquilado' );
            }
        });
        $('.wpml-lang-ca .casa-grid .tag').each(function () {
            if ($(this).text() == 'sold'){
                $(this).text( 'Venut' );
            } else if ($(this).text() == 'rented'){
                $(this).text( 'Llogat' );
            }
        });
    });
	
</script>

<?php
get_footer();
